#!/bin/bash

BRANCH="HEAD"

#if [ "$#" -ne 1 ]; then
#    echo ""
#    echo "usage: git-reset <BRANCH>"
#    echo ""
#    
#    exit 1
#fi

if [ "$#" -eq 1 ]; then
    echo "Using branch '$1'..."
    BRANCH="$1"
fi

git reset --hard "$BRANCH"
git checkout main
git pull

echo ""
echo "You are now setup to make changes to the project."
echo "When you have made your changes, run git-push.sh <NEW_BRANCH_NAME>"
echo ""
echo "Thank you for using the git-reset.sh script."
echo ""

exit 0
