# Lookup API

The `Lookup` API is a scaled down version of what Jaroslav Turlach created for 
the NetBeans Platform. Its sole purpose is to ease the burden of using the
`[java.util.ServiceLoader](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/ServiceLoader.html)
class, and to add some features that were missing from that tool.

## `ServiceLoader` Usage

To use the `ServiceLoader` class, typically one would need to write two or more
lines of code:

```java
// Simplest use:
ServiceLoader<MyService> ldr = ServiceLoader.load(MyService.class);
MyService mySvc = ldr.findFirst().get();
```

For more complex service searches, many more lines of code could be needed, but
for most use-cases, even this is more than should be required. This is where our
`Lookup` comes into play. Instead of the above example, one could simply write:

```java
// Typical use:
MyService mySvc = Lookup.getDefault().lookup(MyService.class);
```

The default instance of `Lookup` is usually enough for most scenarios, but the
`Lookup` class provides a method for using custom `Lookup` implementations:

```java
MyLookup lkp = Lookup.getLookup(MyLookup.class);
```

In this way, a developer may create their own custom lookups for their needs. We
believe that this makes everyone's life just a little bit easier.

## Added Features

When using `ServiceLoader` with a class that has already been brought into 
service within an application, without taking some special steps, `ServiceLoader`
will fail to do what one expects. For example, if a development team has created
an interface that is implemented by the main window of their project, and this
interface is used to display a message on the main window's statusbar, one would
expect to be able to display the message from anywhere like this (assuming the
interface is named `StatusDisplayer`):

```java
ServiceLoader<StatusDisplayer> ldr = ServiceLoader.load(StatusDisplayer.class);
StatusDisplayer disp = ldr.findFirst().get();
disp.setMessage("Hello, using ServiceLoader");
```

However, if this code was used in a project that is *not* a modules-based project,
i.e., not being designed in a *Project Jigsaw* manner, then the above code would
***not* in fact** display the message as was intended. In fact, the message that
was being displayed would simply go into the ether, and no errors would occur.

This is because the `ServiceLoader` class in a standard JAR or extensible
application does not seek out anything other than a default, no-args constructor
for initializing the requested service.

Now, if you are in a *Project Jigsaw* style modular project, you could add the
static `provider` method to your service implementation class and the above code
would work as expected.

With all of this in mind, you can now see what is wrong with the code in the
example above. Instead of placing that text in the statusbar of the main window,
the `ServiceLoader` would instantiate a whole new instance of the main window
class and call *that* instance's `setMessage` method. The reason the message was
not visible in the statusbar is because the main window instance to which it was
sent is not visible.

### `Lookup` Helps with This

This is one of the shortcomings of `ServiceLoader` which drove PekinSOFT Systems
to create this Lookup API. With `Lookup` (even the default instance), one can
reasonably expect that if their service implementation class provides a public 
static `getInstance` method, the `Lookup` will use it.

### Another Added Feature

The Lookup API provides not only the `Lookup` abstract class and a default
implementation for your use, it also provides a special `Lookup` that allows you
to configure a certain directory or group of directories in which this `Lookup`
must concentrate its searches.

This special `Lookup` implementation is called `PluginLookup`. It does not 
define exactly what a "plugin" is, that is left up to the consumer. What the
`PluginLookup` is best suited for, however, is loading JAR files from a certain
location in the filesystem that is established by the consumer who retrieves that
`Lookup` implementation. This can be handy for when you are building an
extensible application that allows third-parties to provide "plugins" to your
application. Typically, the primary developer will have developed some rules as
to where those plugins will be located at run time. Then, using the `PluginLookup`,
the application will be able to determine if there are any plugins installed on
the user's system. If so, the developer could use the `PluginLookup` to load
them into the running application.

## Conclusion

While `ServiceLoader` is useful, and all of us were grateful when it was moved
from being an internal utility to one we could all use, it still lacks something
that we need. Mixed with the fact that it is a final class, and so cannot be
extended, we have each sought to solve our needs in many ways. Hopefully, `Lookup`
will be able to solve at least a few more of the issues with `ServiceLoader` for
you and your project.
