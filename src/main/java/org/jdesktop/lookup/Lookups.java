/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   LookupAPI
 *  Class      :   Lookups.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 17, 2023
 *  Modified   :   Jan 17, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 17, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.lookup;

/**
 * Factory class that provides only static methods by which {@code Lookup}s may be
 * retrieved for use.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class Lookups {
    
    private static Lookup defaultLookup = null;
    private static Lookup pluginLookup = null;
    
    private Lookups () { /* No instantiation allowed */ }
    
    /**
     * Retrieves the default {@link org.jdesktop.lookup.Lookup DefaultLookup} 
     * service instance.
     * <p>
     * The {@code DefaultLookup} takes into account the fact that the <em>
     * Singleton Design Pattern</em> exists. This means that as a service
     * provider class is located, it is checked for the existence of a {@code 
     * public static Object getInstance()} method. If that method does not exist,
     * the class is checked to see if it is compliance with the {@link 
     * java.util.ServiceLoader ServiceLoader}'s requirement for a {@code public
     * static Object provider()} method to be present. If neither of these two
     * methods exist on the class, then the public default, no-args constructor
     * is used to get an instance of the service provider.</p>
     * 
     * @return the default {@code DefaultLookup}
     */
    public static Lookup getDefault() {
        if (defaultLookup == null) {
            defaultLookup = new DefaultLookup();
        }
        
        return defaultLookup;
    }

}
