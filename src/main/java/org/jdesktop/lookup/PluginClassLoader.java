/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos
 *  Class      :   PluginClassLoader.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 16, 2023
 *  Modified   :   Jan 16, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 16, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.lookup;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
class PluginClassLoader extends URLClassLoader {
    
    private static final List<String> sharedPackages
            = Arrays.asList(
                    "org.jdesktop",
                    "com.pekinsoft",
                    "com.javadocking",
                    "javax.help",
                    "com.jtattoo",
                    "javax.activation",
                    "javax.mail"
            );
    
    private ClassLoader parent;
    
    public PluginClassLoader (URL[] urls, ClassLoader parent) {
        super(urls);
        this.parent = parent;
    }

    @Override
    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        // Has the class been loaded already?
        Class<?> loaded = findLoadedClass(name);
        
        if (loaded == null) {
            final boolean isSharedClass = sharedPackages.stream().anyMatch(name::startsWith);
            if (isSharedClass) {
                loaded = parent.loadClass(name);
            } else {
                loaded = super.loadClass(name, resolve);
            }
        }
        
        if (resolve) {
            resolveClass(loaded);
        }
        
        return loaded;
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        return loadClass(name, false);
    }

}
