/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   Application
 *  Class      :   DefaultLookup.java
 *  Author     :   Sean Carrick
 *  Created    :   Dec 12, 2022
 *  Modified   :   Dec 12, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Dec 12, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package org.jdesktop.lookup;

import java.lang.System.Logger;
import java.lang.module.ModuleFinder;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.Set;

/**
 * {@code Lookup} is a small interface that mimics the most basic functionality
 * of the NetBeans Platform's{@code Lookup} feature. This class provides four (4)
 * methods, as required by the {@code Lookup} interface, but only truly uses two (2)
 * of them:
 * <ul>
 * <li>{@code lookup(Class type)} &mdash; finds the specified class type in the class
 * path and returns the first instance located. </li>
 * <li>{@code lookup(Class type, boolean refreshCache)} &mdash; finds all instances
 * of the specified service provider. If {@code refreshCache} is {@code true},
 * then the entire lookup is reexecuted and the cache is ignored and reloaded.</li>
 * <li>{@code lookupAll(Class type)} &mdash; finds the specified class type in the 
 * class path and returns a collection of instances found. </li>
 * <li>{@code lookupAll(Class type, boolean refreshCache)} &mdash; <strong><em>
 * unused</em></strong> this <strong>is not</strong> a caching lookup. Calling 
 * this method does exactly the same thing as calling {@code lookupAll(Class type)}.
 * </li></ul>
 * <p>
 * For all service types being looked up, they must be properly declared in
 * accordance with the standard Java extension mechanism. Therefore, the 
 * following rules apply:</p>
 * <ul>
 * <li>When on the {@code classpath}:
 *    <ul><li>The service provider <em>must</em> be registered in the JAR file's 
 * {@code META-INF/services}{ folder.</li>
 *    <li>This registration <em>must</em> be in a file named with the full-qualified name
 * of the service definition interface or class.</li>
 *    <li>The service provider's fully-qualified class name <em>must</em> be entered in
 * this file, one provider class per line.</li>
 * </ul>
 * <li>When on the {@code module-path}:
 *    <ul><li>The service provider's module <em>must</em> declare {@code provides} for the specific
 * service type definition interface or class.</li>
 *    <li>The service provider's module <em>must</em> be {@code open}, or declare the package
 * in which the service provider {@code opens} to the {@code Lookup}.</li>
 *    <li>The class looking up the service provider's module <em>must</em> declare {@code uses}
 * the fully-qualified class name of the service definition interface or class.
 *</li></ul></li></ul>
 * 
 * @param <T> the specific type being looked up
 * 
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class DefaultLookup<T> implements Lookup {
    
    private static Logger logger = System.getLogger(DefaultLookup.class.getName());
    
    /**
     * A map of instances, keyed by type.
     */
    private final Map<Class<T>, List<T>> cache;
    /**
     * A string array to hold the various parts of the class path, if we are on
     * the class path.
     */
    private final String[] classPaths;
    /**
     * The ClassLoader we will use for locating providers.
     */
    private final ClassLoader classLoader;
    
    /**
     * A ModuleFinder is a tool for locating modules and getting information
     * about them and their contents.
     */
    private ModuleFinder finder;
    /**
     * An array of strings to hold the various module paths that may be set if
     * the calling application is a modular one.
     */
    private String[] modulePaths;
    
    
    /**
     * Constructs the {@code DefaultLookup} and prepares it for use.
     */
    DefaultLookup() {
        logger.log(Logger.Level.INFO, "Configuring the DefaultLookup");
        // We will start by getting the module path from the System. If this is
        //+ being called from a non-modular application, this will be null.
        String modulePath = System.getProperty("jdk.module.path");
        if (modulePath != null) {
            modulePaths = modulePath.split(System.getProperty("path.separator"));
        
            // Strings won't work for getting the ModuleFinder, so we need to 
            //+ convert those strings into Paths.
            Path[] paths = null;
            if (modulePaths.length > 0) {
                paths = new Path[modulePaths.length];
                for (int x = 0; x < modulePaths.length; x++) {
                    paths[x] = Path.of(modulePaths[x]);
                }
            }
            
            // Using the various module paths (split on the path separator), we
            //+ get a ModuleFinder object to use for locating the various modules
            //+ of a modular application.
            finder = paths != null ? ModuleFinder.of(paths) : null;
        }

        // If the calling application is non-modular, the class path will not be
        //+ null, but if it is a modular program, it will be.
        classPaths = System.getProperty("java.class.path")
                .split(System.getProperty("path.separator"));
        
        String msg = (classPaths == null || classPaths.length == 0)
                ? "We are working with the module-path and the classlaoder: {0}"
                : "We are working with the classpath and the classloader: {0}";
        
        // Regardless of whether we are on a class path or a module path, the
        //+ System ClassLoader will suffice.
        classLoader = ClassLoader.getSystemClassLoader();
        
        logger.log(Logger.Level.INFO, msg, classLoader);
        
        cache = new HashMap<>();
    }

    @Override
    public T lookup(Class type, boolean refreshCache) {
        // Quickly return if cache refreshing was not requested and the sought
        //+ type is already in the cached.
        if (!refreshCache && cache.containsKey(type)) {
            Object o = cache.get(type).get(0);
            logger.log(Logger.Level.INFO, "Returning {0} from the cache", o);
            return (T) o;  // The first instance.
        }
        
        logger.log(Logger.Level.INFO, "Attempting to locate a provider of {0}", type);
        T ti = (T) ServiceLoader.load(type).findFirst().get();
        if (ti != null) {
            Class cls = ti.getClass();
            logger.log(Logger.Level.DEBUG, "Located a provider of {0}. Checking "
                    + "if it is a singleton or offers a provides method.", type);
            if (LookupHelper.isSingleton(cls)) {
                T o = (T) LookupHelper.getInstance(cls);
                logger.log(Logger.Level.INFO, "{0} is a singleton with a "
                        + "getInstance method. Returning the instance {1}", cls, o);
                return o;
            } else if (LookupHelper.isProvider(ti.getClass())) {
                T o = (T) LookupHelper.getProvider(cls);
                logger.log(Logger.Level.INFO, "{0} is a singleton with a "
                        + "provider method. Returning the instance {1}", cls, o);
                return o;
            } else {
                logger.log(Logger.Level.WARNING, "{0} was not a singleton, so "
                        + "returning the instance that was constructed.", cls);
                return ti;
            }
        } else {
            throw new LookupException("Unable to locate any providers for " + type);
        }
    }

    @Override
    public Collection<T> lookupAll(Class type, boolean refreshCache) {
        // Quickly return if we are not refreshing the cache and the service
        //+ provider is already in it.
        if (!refreshCache && cache.containsKey(type)) {
            return cache.get(type);
        }
        
        List<T> found = new ArrayList<>();
        Set<T> testSet = fallbackAll(type);
        for (T ti : testSet) {
            Class cls = ti.getClass();
            
            if (LookupHelper.isSingleton(cls)) {
                T o = (T) LookupHelper.getInstance(cls);
                found.add(o);
            } else if (LookupHelper.isProvider(cls)) {
                T o = (T) LookupHelper.getProvider(cls);
                found.add(o);
            } else {
                found.add(ti);
            }
        }
        
        // Add the list of service providers found to the cache.
        if (cache.containsKey(type)) {
            cache.remove(type);
        }
        cache.put(type, found);
        
        // Return the located service providers.
        return found;
    }

    /**
     * {@code fallback} is a "last-resort" option for custom {@code Lookup} subclasses. This
     * method resorts to the {@link java.util.ServiceLoader
     * ServiceLoader} class for locating service providers. When subclassing
     * {@code DefaultLookup}, the subclass will want to do everything possible to
     * avoid calling this method. If everything possible has not been attempted,
     * the the subclass' {@link #lookup(java.lang.Class) lookup(Class)} method
     * is no different than that of the {@code DefaultLookup} instance.
     *
     * @param <T> the class type being looked up, which is an instance of
     * {@code DefaultLookup}
     * @param type the class being looked up, which is an instance of
     * {@code DefaultLookup}
     * @return the first instance of the sought provider
     */
    protected <T> T fallback(Class<T> type) {
        Iterator<T> ldr = ServiceLoader
                .load(type, ClassLoader.getSystemClassLoader())
                .iterator();
        return ldr.hasNext() ? ldr.next() : null;
    }

    /**
     * {@code fallbackAll} is a "last-resort" option for custom {@code Lookup} subclasses.
     * This method resorts to the {@link java.util.ServiceLoader
     * ServiceLoader} class for locating service providers. When subclassing
     * {@code DefaultLookup}, the subclass will want to do everything possible to
     * avoid calling this method. If everything possible has not been attempted,
     * the subclass' {@link #lookupAll(java.lang.Class) lookupAll(Class)} method
     * is no different than that of the {@code DefaultLookup} instance.
     *
     * @param <T> the class type being looked up
     * @param type the class being looked up
     * @return a collection of the requested service providers
     */
    protected <T> Set<T> fallbackAll(Class<T> type) {
        Set<T> providers = new HashSet<>();
        for (T t : ServiceLoader.load(type, ClassLoader.getSystemClassLoader())) {
            providers.add(t);
        }

        return providers;
    }

}
