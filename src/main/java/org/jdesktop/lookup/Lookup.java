/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   LookupAPI
 *  Class      :   Lookup.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 17, 2023
 *  Modified   :   Jan 17, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 17, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.lookup;

import java.util.Collection;

/**
 * {@code Lookup} is a small abstract class that mimics the most basic 
 * functionality of the NetBeans Platform's{@code Lookup} feature. This class
 * provides two (2) abstract and two (2) static methods for use in applications:
 * <ul>
 * <li>{@code lookup(Class type)} &mdash; finds the specified class type in the
 * class path and returns the first instance located.</li>
 * <li>{@code lookupAll(Class type)} &mdash; finds the specified class type in
 * the class path and returns a collection of instances found.</li>
 * <li>{@code getDefault()} &mdash; retrieves the default implementation of the
 * {@code DefaultLookup} abstract class, which is useful for most purposes.</li>
 * <li>{@code getLookup(Class lookup)} &mdash; retrieves the first implementation
 * of the specific {@code DefaultLookup} subclass specified, which is useful for having
 * other classes and/or Swing components be able to provide a {@code DefaultLookup}.
 * </li></ul>
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public interface Lookup<T> {
    
    /**
     * Retrieves a provider instance of the specified service type.
     * 
     * @param type the class of the type being looked up
     * @return the first instance of the sought service provider
     * @throws IllegalArgumentException if {@code type} is {@code null}
     */
    default T lookup(Class<T> type) {
        return lookup(type, false);
    }
    
    /**
     * Retrieves a provider instance of the specified service type.
     * 
     * If the {@code refreshCache} parameter has the value {@code true}, then the cache 
     * <em>is not</em> checked for an instance first. Instead, the entire lookup is
     * reexecuted for the specified service type and the cache is refreshed.
     * 
     * @param type the class of the type being looked up
     * @param refreshCache {@code true} to refresh the cache; {@code false} to not refresh
     * @return the first instance of the sought service provider
     * @throws IllegalArgumentException if {@code type} is {@code null}
     */
    T lookup(Class<T> type, boolean refreshCache);
    
    /**
     * Retrieves a collection of all instances of the specified service type.
     * 
     * @param type the class of the type being looked up
     * @return a collection of instances of the service sought
     * @throws IllegalArgumentException if {@code type} is {@code null}
     */
    default Collection<? extends T> lookupAll(Class<T> type) {
        return lookupAll(type, false);
    }
    
    /**
     * Retrieves a collection of all instances of the specified service type.
     * 
     * If the {@code refreshCache} parameter has the value {@code true}, then the cache 
     * <em>is not</em> checked for a collection of instances first. Instead, the entire
     * lookup is reexecuted for the specified service type and the cache is
     * refreshed.
     * 
     * @param type the class of the type being looked up
     * @param refreshCache {@code true} to refresh the cache; {@code false} to not refresh
     * @return a collection of instances of the service sought
     * @throws IllegalArgumentException if {@code type} is {@code null}
     */
    Collection<? extends T> lookupAll(Class<T> type, boolean refreshCache);
    
}
