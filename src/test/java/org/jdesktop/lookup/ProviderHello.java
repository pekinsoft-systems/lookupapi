/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   LookupAPI
 *  Class      :   ProviderHello.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 18, 2023
 *  Modified   :   Jan 18, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 18, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package org.jdesktop.lookup;

import org.jdesktop.test.spi.HelloService;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class ProviderHello implements HelloService {
    
    public static final String SHOULD_BE = "Hello from provider";
    
    private static ProviderHello instance = null;
    private String hello = null;

    public ProviderHello() {
        hello = "Hello from the default constructor";
    }

    public ProviderHello(String hello) {
        this.hello = hello;
    }

    public static ProviderHello provider() {
        if (instance == null) {
            instance = new ProviderHello("Hello from provider");
        }
        return instance;
    }

    @Override
    public String getHelloMessage() {
        return hello;
    }
}
