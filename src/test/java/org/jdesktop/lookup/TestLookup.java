/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   LookupAPI
 *  Class      :   TestLookup.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 18, 2023
 *  Modified   :   Jan 18, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 18, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package org.jdesktop.lookup;

import java.lang.System.Logger;
import java.util.List;
import junit.framework.TestCase;
import org.jdesktop.test.spi.HelloService;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class TestLookup extends TestCase {
    
    private static Logger logger = System.getLogger(TestLookup.class.getName());
    
    public TestLookup() {

    }
    
    public void testLookup() {
        HelloService hello 
                = (HelloService) Lookups.getDefault().lookup(HelloService.class);
        logger.log(Logger.Level.INFO, "Located the service provider \"{0}\"",
                hello.getClass().getSimpleName());
        if (hello instanceof ConstructedHello) {
            assertEquals(ConstructedHello.SHOULD_BE, hello.getHelloMessage());
        } else if (hello instanceof InstanceHello) {
            assertEquals(InstanceHello.SHOULD_BE, hello.getHelloMessage());
        } else if (hello instanceof ProviderHello) {
            assertEquals(ProviderHello.SHOULD_BE, hello.getHelloMessage());
        }
    }

    public void testLookupAll() {
        List<HelloService> hellos
                = (List<HelloService>) Lookups.getDefault()
                        .lookupAll(HelloService.class);
        
        logger.log(Logger.Level.INFO, "Located {0} service providers.", hellos.size());
        
        for (HelloService hello : hellos) {
            logger.log(Logger.Level.INFO, hello.getHelloMessage());
            if (hello instanceof ConstructedHello) {
                assertEquals(ConstructedHello.SHOULD_BE, hello.getHelloMessage());
            } else if (hello instanceof InstanceHello) {
                assertEquals(InstanceHello.SHOULD_BE, hello.getHelloMessage());
            } else if (hello instanceof ProviderHello) {
                assertEquals(ProviderHello.SHOULD_BE, hello.getHelloMessage());
            }
        }
    }

    @Override
    protected void runTest() throws Throwable {
        testLookupAll();
        
        testLookup();
    }

}
